//
//  ColorUtil.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 24/10/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import UIKit

struct ColorPercentage {
    let R : Double
    let G : Double
    let B : Double
    let A : Double
    
    init(RInt: Int, GInt: Int, BInt: Int, AInt: Int ) {
        R = Double(RInt) / 255.0
        G = Double(GInt) / 255.0
        B = Double(BInt) / 255.0
        A = Double(AInt) / 255.0
        
    }
}


// Create UIColor from color channel values in Int format
func createUIColorFromIntValues(RInt: Int, GInt: Int, BInt: Int, AInt: Int) -> UIColor {
    return UIColor.init(red: CGFloat(Double(RInt)/255.0), green: CGFloat(Double(GInt)/255.0), blue: CGFloat(Double(BInt)/255.0), alpha: CGFloat(Double(AInt)/255.0))
}


