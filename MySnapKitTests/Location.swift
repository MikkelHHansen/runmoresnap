//
//  Location.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 05/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import CoreData

class Location {
    
    init () {
        timestamp = NSDate()
        latitude = 0
        longitude = 0
        speed = 0
    }
    
    var timestamp: NSDate
    var latitude : Double
    var longitude: Double
    var speed: Double
    
}