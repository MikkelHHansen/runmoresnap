//
//  Interval.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 12/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import CoreData

class Interval {

    var type: String
    var length: Int
    var title: String
    
    init(type: String, length: Int, title: String) {
        self.type = type
        self.length = length
        self.title = title
    }
    
}