//
//  IntervalViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 07/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class IntervalViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var runPickerView = UIPickerView()
    var walkPickerView = UIPickerView()
    var superview = UIView()
    var runLabel = UILabel()
    var walkLabel = UILabel()
    var startRunButton = UIButton()
    var stopRunButton = UIButton()
    var countdownLabel = UILabel()
    
    var runGPS = GPSCore()
    var timer = NSTimer()
    var pickerViewDataArray = [Int]()
    var runArray = [Run]()
    var walkArray = [Run]()
    
    var runMinutes   = 0
    var runSeconds   = 0
    var walkMinutes  = 0
    var walkSeconds  = 0
    var runTimeInterval  = 0
    var walkTimeInterval = 0
    var numOfIntervals = 0
    var isRunning = false
    var isStartPressed = false
    
//    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if component == 0 {
//            return "\(pickerViewDataArray[row]) min"
//        }
//        if component == 1 {
//            return "\(pickerViewDataArray[row]) sec"
//        }
//        if component == 2 {
//            return "\(pickerViewDataArray[row]) intervals"
//        } else {
//            return ""
//        }
//    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewDataArray.count
    }
    
    // num of wheels in pickerview
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if pickerView.tag == 1 {
            return 3
        } else {
            return 2
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            runMinutes = pickerView.selectedRowInComponent(0)
            runSeconds = pickerView.selectedRowInComponent(1)
            numOfIntervals = pickerView.selectedRowInComponent(2)
            updateRunLabel()
            updateCountdownLabel()
            runTimeInterval = setCountdownInterval(runMinutes, seconds: runSeconds)
        } else if pickerView.tag == 2 {
            walkMinutes = pickerView.selectedRowInComponent(0)
            walkSeconds = pickerView.selectedRowInComponent(1)
            updateWalkLabel()
            walkTimeInterval = setCountdownInterval(walkMinutes, seconds: walkSeconds)
        } else {
            print("PickerView selection error")
        }
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        if pickerView.tag == 1 && component == 0 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataArray[row]) min"
            } else {
                pickerLabel.text = "\(pickerViewDataArray[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 20)
        } else if pickerView.tag == 1 && component == 1 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataArray[row]) sec"
            } else {
                pickerLabel.text = "\(pickerViewDataArray[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 20)
        } else if pickerView.tag == 1 && component == 2 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataArray[row]) intervals"
            } else {
                pickerLabel.text = "\(pickerViewDataArray[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 20)
        }
        
        if pickerView.tag == 2 && component == 0 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataArray[row]) min"
            } else {
                pickerLabel.text = "\(pickerViewDataArray[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 20)
        } else if pickerView.tag == 2 && component == 1 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataArray[row]) sec"
            } else {
                pickerLabel.text = "\(pickerViewDataArray[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 20)
        }
        return pickerLabel
    }
    
    func fillDataArray() {
        for num in 0...59 {
            pickerViewDataArray.append(num)
        }
    }
    
    func updateRunLabel() {
        runLabel.text = "Run for \(runMinutes) minutes & \(runSeconds) seconds."
    }
    
    func updateWalkLabel() {
        walkLabel.text = "Walk for \(walkMinutes) minutes & \(walkSeconds) seconds."
    }
    
    func updateCountdownLabel() {
        let runMin = String(format: "%02d", runMinutes)
        let runSec = String(format: "%02d", runSeconds)
        countdownLabel.text = "\(runMin):\(runSec)"
    }
    
    func startRunButtonAction(sender: UIButton!) {
        if !isStartPressed {
            isStartPressed = true
            startRunButton.setTitle("Pause", forState: .Normal)
            setPickerViewInteraction(false)
            isRunning = true
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(IntervalViewController.updateTimer), userInfo: nil, repeats: true)
            runGPS.startRunning()
        } else if isStartPressed {
            isStartPressed = false
            startRunButton.setTitle("Resume", forState: .Normal)
            timer.invalidate()
            runGPS.stopRunning()
        }
    }
    
    func stopRunButtonAction(sender: UIButton!) {
        setPickerViewInteraction(true)
        timer.invalidate()
        countdownLabel.text = "00:00"
        runGPS.stopRunning()
        runGPS.flushLocations()
        startRunButton.setTitle("Start", forState: .Normal)
    }
    
    func setCountdownInterval(minutes: Int, seconds: Int) -> Int {
        return minutes * 60 + seconds
    }
    
    func setPickerViewInteraction(bool: Bool) {
        runPickerView.userInteractionEnabled = bool
        walkPickerView.userInteractionEnabled = bool
    }
    
    func updateTimer() {
        
        if walkTimeInterval == -1 {
            isRunning = true
            walkTimeInterval = setCountdownInterval(walkMinutes, seconds: walkSeconds)
            walkArray.append(runGPS.getRunData("\(walkMinutes):\(walkSeconds)"))
            runGPS.flushLocations()
            numOfIntervals -= 1
            print("(Walk End) Intervals: \(numOfIntervals)")
        }
        
        if runTimeInterval == -1 {
            runTimeInterval = setCountdownInterval(runMinutes, seconds: runSeconds)
            isRunning = false
            runArray.append(runGPS.getRunData("\(runMinutes):\(runSeconds)"))
            runGPS.flushLocations()
            numOfIntervals -= 1
            print("(Run End) Intervals: \(numOfIntervals)")
        }
        
        if isRunning && runTimeInterval > -1 {
            let minutes = String(format: "%02d", runTimeInterval / 60)
            let seconds = String(format: "%02d", runTimeInterval % 60)
            countdownLabel.text = "\(minutes):\(seconds)"
            runTimeInterval -= 1
        } else if walkTimeInterval > -1 {
            let minutes = String(format: "%02d", walkTimeInterval / 60)
            let seconds = String(format: "%02d", walkTimeInterval % 60)
            countdownLabel.text = "\(minutes):\(seconds)"
            walkTimeInterval -= 1
        } else {
            print("Why am I here?")
        }
        
        if numOfIntervals == 0 {
            timer.invalidate()
            runGPS.stopRunning()
            runGPS.flushLocations()
            setPickerViewInteraction(true)
            countdownLabel.text = "00:00"
            startRunButton.setTitle("Start", forState: .Normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillDataArray()
        runGPS.calibrateGPS()
        
        runPickerView.dataSource = self
        walkPickerView.dataSource = self
        runPickerView.delegate = self
        walkPickerView.delegate = self
        runPickerView.tag = 1
        walkPickerView.tag = 2
        
        superview = self.view
        superview.backgroundColor = UIColor.whiteColor()
        
        superview.addSubview(runPickerView)
        
        runPickerView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width)
            make.height.equalTo(superview.bounds.height / 4)
            make.top.equalTo(superview.snp_top).offset(20)
        }
        
        superview.addSubview(walkPickerView)
        
        walkPickerView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width)
            make.height.equalTo(superview.bounds.height / 4)
            make.top.equalTo(runPickerView.snp_bottom)
        }
        
        runLabel.text = ""
        runLabel.textAlignment = NSTextAlignment.Left
        superview.addSubview(runLabel)
        
        runLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width)
            make.height.equalTo(25)
            make.top.equalTo(walkPickerView.snp_bottom).offset(10)
            make.left.equalTo(superview.snp_left).offset(5)
        }
        
        walkLabel.text = ""
        walkLabel.textAlignment = NSTextAlignment.Left
        superview.addSubview(walkLabel)
        
        walkLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width)
            make.height.equalTo(25)
            make.top.equalTo(runLabel.snp_bottom).offset(10)
            make.left.equalTo(superview.snp_left).offset(5)
        }
        
        countdownLabel.text = "00:00"
        countdownLabel.font = countdownLabel.font?.monospacedDigitFont
        countdownLabel.textAlignment = NSTextAlignment.Center
        superview.addSubview(countdownLabel)
        
        countdownLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(25)
            make.top.equalTo(walkLabel.snp_bottom).offset(50)
            make.left.equalTo(superview.snp_left).offset((superview.bounds.width / 2) - 50)
        }
        
        startRunButton.layer.cornerRadius = 33
        startRunButton.setTitle("Start", forState: .Normal)
        startRunButton.backgroundColor = UIColor.greenColor()
        startRunButton.addTarget(self, action: #selector(IntervalViewController.startRunButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(startRunButton)
        
        startRunButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(66)
            make.height.equalTo(66)
            make.right.equalTo(superview.snp_right).offset(-20)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
        }
        
        stopRunButton.layer.cornerRadius = 33
        stopRunButton.setTitle("Stop", forState: .Normal)
        stopRunButton.backgroundColor = UIColor.redColor()
        stopRunButton.addTarget(self, action: #selector(IntervalViewController.stopRunButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(stopRunButton)
        
        stopRunButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(66)
            make.height.equalTo(66)
            make.left.equalTo(superview.snp_left).offset(20)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}