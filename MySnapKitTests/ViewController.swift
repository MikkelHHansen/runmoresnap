//
//  ViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 25/08/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    var superview = UIView()
    var runButton = UIButton()
    var intervalButton = UIButton()
    var loginButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        superview = self.view
        
        //let http = HttpRequest()
        //http.basicAsyncHTTP()
        //print(http.count)
        
        intervalButton.layer.cornerRadius = 33
        intervalButton.setTitle("Interval", forState: .Normal)
        intervalButton.backgroundColor = UIColor.blueColor()
        intervalButton.addTarget(self, action: #selector(ViewController.intervalButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(intervalButton)
        
        intervalButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(intervalButton.layer.cornerRadius * 2)
            make.height.equalTo(intervalButton.layer.cornerRadius * 2)
            make.left.equalTo(superview.snp_left).offset(20)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
        }
        
        runButton.layer.cornerRadius = 33
        runButton.setTitle("Run", forState: .Normal)
        runButton.backgroundColor = UIColor.blackColor()
        runButton.addTarget(self, action: #selector(ViewController.runButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(runButton)
        
        runButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(runButton.layer.cornerRadius * 2)
            make.height.equalTo(runButton.layer.cornerRadius * 2)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
            make.right.equalTo(superview.snp_right).offset(-20)
        }
        
//        loginButton.layer.cornerRadius = 33
//        loginButton.setTitle("Login", forState: .Normal)
//        loginButton.backgroundColor = UIColor.grayColor()
//        loginButton.addTarget(self, action: #selector(ViewController.loginButtonAction(_:)), forControlEvents: .TouchUpInside)
//        superview.addSubview(loginButton)
//        
//        loginButton.snp_makeConstraints { (make) -> Void in
//            make.width.height.equalTo(66)
//            make.center.equalTo(superview)
//        }
    }
    
    func loginButtonAction(sendeR: UIButton!) {
        let logVC = LoginViewController()
        self.presentViewController(logVC, animated: true, completion: nil)
    }
    
    func runButtonAction(sender: UIButton!) {
        let runVC = RunViewController()
        self.presentViewController(runVC, animated: true, completion: nil)
    }
    
    func intervalButtonAction(sender: UIButton!) {
        let intervalVC: NewIntervalViewController = NewIntervalViewController()
        self.presentViewController(intervalVC, animated: true, completion: nil)
    }
}

