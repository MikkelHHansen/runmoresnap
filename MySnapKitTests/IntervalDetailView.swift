//
//  IntervalDetailView.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 13/10/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class IntervalDetailView: NSObject {
    
    private var superview: UIView?
    var topView = UIView()
    var detailView = UIView()
    var closeDetailButton = UIButton()
    var intervalTypeLabel = UILabel()
    var intervalDurationLabel = UILabel()
    var intervalDistanceLabel = UILabel()
    
    override init() {
        super.init()
    }
    
    func setSuperView(view: UIView) {
        superview = view
    }
    
    func constructDetailsGUI(intervalLength: Double, intervalType: String, intervalDuration: Int) {
        topView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        superview!.addSubview(topView)
        
        topView.snp_makeConstraints { (make) -> Void in
            make.size.equalTo(superview!)
            make.center.equalTo(superview!)
        }
        
        detailView.backgroundColor = UIColor.whiteColor()
        detailView.layer.cornerRadius = 11
        topView.addSubview(detailView)
        
        detailView.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(topView.bounds.height / 2.5)
            make.width.equalTo(topView.bounds.width / 1.5)
            make.center.equalTo(topView)
        }
        
        intervalTypeLabel.textAlignment = .Center
        intervalTypeLabel.text = "Interval type: \(intervalType)"
        detailView.addSubview(intervalTypeLabel)
        
        intervalTypeLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(detailView.snp_top).offset(10)
        }
        
        intervalDurationLabel.textAlignment = .Center
        intervalDurationLabel.text = "Interval duration: \(intervalDuration)"
        detailView.addSubview(intervalDurationLabel)
        
        intervalDurationLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(intervalTypeLabel.snp_bottom).offset(10)
        }
        
        intervalDistanceLabel.textAlignment = .Center
        intervalDistanceLabel.text = "Interval distance: \(intervalLength.roundToPlaces(2))"
        detailView.addSubview(intervalDistanceLabel)
        
        intervalDistanceLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(intervalDurationLabel.snp_bottom).offset(10)
        }
        
        closeDetailButton.setTitle("X", forState: .Normal)
        closeDetailButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        closeDetailButton.addTarget(self, action: #selector(self.closeDetailButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        detailView.addSubview(closeDetailButton)
        
        closeDetailButton.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(detailView).offset(5)
            make.right.equalTo(detailView).offset(-5)
            make.width.height.equalTo(20)
        }
    }
    
    func closeDetailButtonAction(sender: UIButton!) {
        superview = nil
        topView.removeFromSuperview()
    }
}
