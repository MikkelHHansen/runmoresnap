//
//  Run.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 05/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import CoreData

class Run {
    
    init () {
        duration = ""
        distance = 0
        timestamp = NSDate()
        locations = [Location]()
    }
    
    func flushRun() {
        duration = ""
        distance = 0
        timestamp = NSDate()
        locations.removeAll(keepCapacity: false)
    }
    
    var duration : String
    var distance : Double
    var timestamp: NSDate
    var locations: [Location]
    
}