//
//  HttpRequest.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 26/11/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation

class HttpRequest {
    
    var count = 0
    
    func basicAsyncHTTP() {
        let request = NSMutableURLRequest(URL: NSURL(string: "http://www.learnswiftonline.com/Samples/subway.json")!)
        request.HTTPMethod = "GET"

        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            // check for fundamental networking error
            guard error == nil && data != nil else {
                print("error=\(error)")
                return
            }
            
            // check for http errors
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            // Pure response data
            //let responseString = String(data: data!, encoding: NSUTF8StringEncoding)
            //print("responseString = \(responseString)")
            
            // Convert to JSON, has to be specific for every json response
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options:.AllowFragments)
                if let stations = json["stations"] as? [[String: AnyObject]] {
                    self.count = stations.count
                    for station in stations {
                        if let name = station["stationName"] as? String {
                            if let year = station["buildYear"] as? String {
                                print(name,year)
                            }
                        }
                    }
                }
            } catch {
                print("Error with Json: \(error)")
            }
        }
        task.resume()
    }
}
