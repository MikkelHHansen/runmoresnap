//
//  RunViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 07/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class RunViewController: UIViewController {
    
    var gpsCore = GPSCore()
    
    let startButton = UIButton()
    let stopButton = UIButton()
    var superview = UIView()
    let stopWatch = UILabel()
    let distanceLabel = UILabel()
    let routeButton = UIButton()
    let speedLabel = UILabel()
    let navBar = UIView()
    let navBarTitleButton = UIButton()
    let navBarOptionsButton = UIButton()
    
    let optionsView = UIView()
    var optionsGUIActive = false
    
    let distanceAnnounceSlider = UISlider()
    let distanceAnnounceSwitch = UISwitch()
    let distanceAnnounceLabel = UILabel()
    let distanceMinLabel = UILabel()
    let distanceMaxLabel = UILabel()
    let distanceAnnounceSliderLabel = UILabel()
    
    let halfwayAnnounceSlider = UISlider()
    let halfwayAnnounceSwitch = UISwitch()
    let halfwayAnnounceLabel = UILabel()
    let halfwayMinLabel = UILabel()
    let halfwayMaxLabel = UILabel()
    let halfwayAnnounceSliderLabel = UILabel()
    
    let speechLabel = UILabel()
    let speechSwitch = UISwitch()
    
    var playedAlerts = 0
    var hasPlayedHalfway = false
    var hasPlayedForInterval = [Int]()
    var prefs = NSUserDefaults.standardUserDefaults()
    var startTime = NSTimeInterval()
    var timer = NSTimer()
    var elapsedTime: NSTimeInterval = 0
    var durationString = ""
    var isStartPressed = false
    var pauseTimeInterval: NSTimeInterval = 0
    
    func updateTime() {
        let currentTime = NSDate.timeIntervalSinceReferenceDate()
        elapsedTime = currentTime - startTime + pauseTimeInterval
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (NSTimeInterval(minutes) * 60)
        let seconds = UInt8(elapsedTime)
        elapsedTime -= NSTimeInterval(seconds)
        let fraction = UInt8(elapsedTime * 100)
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let strFraction = String(format: "%02d", fraction)
        
        stopWatch.text = "\(strMinutes):\(strSeconds):\(strFraction)"
        distanceLabel.text = "Distance Traveled: \(gpsCore.distanceTraveled.roundToPlaces(2)) meters."
        speedLabel.text = "Speed: \(gpsCore.lastLocation.speed) m/s."
        
        // TO DO - Test for alerts
        if prefs.boolForKey("UseDistanceAlerts") {
            playSoundAtInterval()
        }
        
        if prefs.boolForKey("UseHalfwayAlerts") {
            playSoundAtHalfway()
        }
    }
    
    func playSoundAtHalfway() {
        let audio = AudioPlayer()
        let value = Int(Float(gpsCore.distanceTraveled) / prefs.floatForKey("HalfwayAlert"))
        if value == 1 && !hasPlayedHalfway {
            // should say something
            if prefs.boolForKey("UseVoiceAlerts") {
                let speechSynth = SpeechUtil()
                let string = "You are now halfway"
                speechSynth.setUtterance(string)
                //speechSynth.utter.rate = 0.25
                speechSynth.speak(speechSynth.utter)
                hasPlayedHalfway = true
            } else {
                //audio.playHalfwayAlertSound()
                audio.playSoundFile("halfalert", ext: "mp3")
                hasPlayedHalfway = true
            }
        }
    }
    
    func playSoundAtInterval() {
        let audio = AudioPlayer()
        // TO DO test for interval
        let value = Int(Float(gpsCore.distanceTraveled) / prefs.floatForKey("DistanceAlert"))
        if !hasPlayedForInterval.contains(value) && value > 0 {
            playedAlerts += 1
            hasPlayedForInterval.append(playedAlerts)
            // should say something
            if prefs.boolForKey("UseVoiceAlerts") {
                let speechSynth = SpeechUtil()
                let string = "You have run \(Int(Float(value) * prefs.floatForKey("DistanceAlert"))) meters"
                //speechSynth.utter.rate = 0.25
                speechSynth.setUtterance(string)
                speechSynth.speak(speechSynth.utter)
            } else {
                //audio.playDistanceAlertSound()
                audio.playSoundFile("distalert", ext: "mp3")
            }
        } else {
            return
        }
    }
    
    func startGettingData() {
        gpsCore.startRunning()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        startTime = NSDate.timeIntervalSinceReferenceDate()
    }
    
    func stopGettingData() {
        gpsCore.stopRunning()
        timer.invalidate()
    }
    
    func startButtonAction(sender: UIButton!) {
        if !isStartPressed {
            print("Starting Updates")
            startButton.setTitle("Pause", forState: .Normal)
            startGettingData()
            isStartPressed = true
        } else if isStartPressed {
            print("Pausing Updates")
            stopGettingData()
            startButton.setTitle("Resume", forState: .Normal)
            isStartPressed = false
            pauseTimeInterval += NSDate.timeIntervalSinceReferenceDate() - startTime
        } else {
            print("Error on start button click")
        }
    }
    
    func stopButtonAction(sender: UIButton!) {
        print("Stopping Updates")
        startButton.setTitle("Start", forState: .Normal)
        stopGettingData()
        pauseTimeInterval = 0
        gpsCore.distanceTraveled = 0
        gpsCore.flushLocations()
        isStartPressed = false
        hasPlayedHalfway = false
        stopWatch.text = "00:00:00"
        distanceLabel.text = "Distance Traveled: 0.0 meters."
        speedLabel.text = "Speed 0.0 m/s."
    }
    
    func routeButtonAction(sender: UIButton!) {
        let mapVC = MapViewController()
        mapVC.setRunData(gpsCore.getRunData(stopWatch.text!))
        self.presentViewController(mapVC, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //gpsCore.calibrateGPS()
        
        superview = self.view
        superview.backgroundColor = UIColor.whiteColor()
        
        navBar.backgroundColor = UIColor.grayColor()
        superview.addSubview(navBar)
        
        navBar.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(60)
            make.top.equalTo(superview.snp_top)
        }
        
        navBarTitleButton.setTitle("RunMore", forState: .Normal)
        navBarTitleButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        navBarTitleButton.sizeToFit()
        navBarTitleButton.addTarget(self, action: #selector(self.navBarTitleButtonAction(_:)), forControlEvents: .TouchUpInside)
        navBar.addSubview(navBarTitleButton)
        
        navBarTitleButton.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(navBar)
        }
        
        navBarOptionsButton.setTitle("Options", forState: .Normal)
        navBarOptionsButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
        navBarOptionsButton.addTarget(self, action: #selector(self.navBarOptionsButtonAction(_:)), forControlEvents: .TouchUpInside)
        navBar.addSubview(navBarOptionsButton)
        
        navBarOptionsButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(75)
            make.height.equalTo(25)
            make.right.equalTo(navBar).offset(-5)
            make.bottom.equalTo(navBar).offset(-5)
        }
        
        startButton.layer.cornerRadius = 66
        startButton.setTitle("Start", forState: .Normal)
        startButton.backgroundColor = UIColor.blackColor()
        startButton.addTarget(self, action: #selector(RunViewController.startButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(startButton)
        
        stopButton.layer.cornerRadius = 66
        stopButton.setTitle("Reset", forState: .Normal)
        stopButton.backgroundColor = UIColor.blackColor()
        stopButton.addTarget(self, action: #selector(RunViewController.stopButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(stopButton)
        
        stopWatch.font = stopWatch.font.fontWithSize(50)
        stopWatch.font = stopWatch.font.monospacedDigitFont
        stopWatch.text = "00:00:00"
        stopWatch.textAlignment = NSTextAlignment.Center
        superview.addSubview(stopWatch)
        
        distanceLabel.text = "Distance: 0.0 meters."
        distanceLabel.textAlignment = NSTextAlignment.Left
        superview.addSubview(distanceLabel)
        
        routeButton.layer.cornerRadius = 33
        routeButton.setTitle("Route", forState: .Normal)
        routeButton.backgroundColor = UIColor.greenColor()
        routeButton.addTarget(self, action: #selector(RunViewController.routeButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        superview.addSubview(routeButton)
        
        routeButton.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(66)
            make.width.equalTo(66)
            make.bottom.equalTo(stopButton.snp_top).offset(-10)
            make.centerX.equalTo(superview)
        }
        
        distanceLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(25)
            make.top.equalTo(superview).offset(100)
            make.left.equalTo(superview).offset(10)
        }
        
        stopWatch.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(50)
            make.center.equalTo(superview)
        }
        
        startButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(66 * 2)
            make.height.equalTo(66 * 2)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
            make.right.equalTo(superview.snp_right).offset(-25)
        }
        
        stopButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(66 * 2)
            make.height.equalTo(66 * 2)
            make.bottom.equalTo(superview.snp_bottom).offset(-20)
            make.left.equalTo(superview.snp_left).offset(25)
        }
        
        speedLabel.textAlignment = .Left
        speedLabel.text = "Speed: 0.0 m/s."
        superview.addSubview(speedLabel)
        
        speedLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(200)
            make.height.equalTo(25)
            make.top.equalTo(distanceLabel.snp_bottom).offset(10)
            make.left.equalTo(superview).offset(10)
        }
    }
    
    func navBarOptionsButtonAction(sender: UIButton!) {
        if !optionsGUIActive {
            createOptionsGUI()
            optionsGUIActive = true
        } else {
            if distanceAnnounceSwitch.on == true {
                prefs.setFloat(getRoundedValue(250, value: distanceAnnounceSlider.value), forKey: "DistanceAlert")
                print("Distance Float: \(prefs.floatForKey("DistanceAlert"))")
                prefs.setBool(true, forKey: "UseDistanceAlerts")
            } else {
                prefs.setBool(false, forKey: "UseDistanceAlerts")
            }
            
            if halfwayAnnounceSwitch.on == true {
                prefs.setBool(true, forKey: "UseHalfwayAlerts")
                prefs.setFloat(getRoundedValue(500, value: halfwayAnnounceSlider.value), forKey: "HalfwayAlert")
                print("Halfway Float: \(prefs.floatForKey("HalfwayAlert"))")
            } else {
                prefs.setBool(false, forKey: "UseHalfwayAlerts")
            }
            
            if speechSwitch.on == true {
                prefs.setBool(true, forKey: "UseVoiceAlerts")
            } else {
                prefs.setBool(false, forKey: "UseVoiceAlerts")
            }
            
            optionsView.removeFromSuperview()
            optionsGUIActive = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func createOptionsGUI() {
        optionsView.backgroundColor = UIColor.whiteColor()
        superview.addSubview(optionsView)
        
        optionsView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(superview).offset(-60)
            make.bottom.equalTo(superview)
        }
        
        distanceAnnounceLabel.text = "Use distance alerts"
        distanceAnnounceLabel.textAlignment = .Left
        optionsView.addSubview(distanceAnnounceLabel)
        
        distanceAnnounceLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.width.equalTo(150)
            make.left.equalTo(optionsView).offset(35)
            make.top.equalTo(optionsView).offset(50)
        }
        
        distanceAnnounceSwitch.on = prefs.boolForKey("UseDistanceAlerts")
        distanceAnnounceSwitch.addTarget(self, action: #selector(self.distanceAnnounceSwitchAction(_:)), forControlEvents: .TouchUpInside)
        optionsView.addSubview(distanceAnnounceSwitch)
        
        distanceAnnounceSwitch.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(distanceAnnounceLabel.snp_right).offset(5)
            make.centerY.equalTo(distanceAnnounceLabel)
        }
        
        distanceAnnounceSlider.maximumValue = 5000
        distanceAnnounceSlider.minimumValue = 250
        distanceAnnounceSlider.value = prefs.floatForKey("DistanceAlert")
        //distanceAnnounceSlider.setMinimumTrackImage(UIImage(named: "slider img1"), forState: .Normal)
        //distanceAnnounceSlider.setMaximumTrackImage(UIImage(named: "slider img2"), forState: .Normal)
        distanceAnnounceSlider.addTarget(self, action: #selector(self.distanceAnnounceSliderAction(_:)), forControlEvents: .TouchDragInside)
        distanceAnnounceSlider.hidden = true
        optionsView.addSubview(distanceAnnounceSlider)
        
        distanceAnnounceSlider.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(optionsView).offset(-120)
            make.centerX.equalTo(optionsView).offset(-12.5)
            make.top.equalTo(distanceAnnounceLabel.snp_bottom).offset(15)
        }
        
        distanceMinLabel.text = "250"
        distanceMinLabel.textAlignment = .Center
        distanceMinLabel.hidden = true
        optionsView.addSubview(distanceMinLabel)
        
        distanceMinLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(40)
            make.right.equalTo(distanceAnnounceSlider.snp_left).offset(-3)
            make.top.equalTo(distanceAnnounceSlider)
        }
        
        distanceMaxLabel.text = "5000"
        distanceMaxLabel.textAlignment = .Center
        distanceMaxLabel.hidden = true
        optionsView.addSubview(distanceMaxLabel)
        
        distanceMaxLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(50)
            make.left.equalTo(distanceAnnounceSlider.snp_right).offset(3)
            make.top.equalTo(distanceAnnounceSlider)
        }
        
        distanceAnnounceSliderLabel.textAlignment = .Center
        distanceAnnounceSliderLabel.text = "\(distanceAnnounceSlider.value) meters."
        distanceAnnounceSliderLabel.hidden = true
        distanceAnnounceSliderLabel.sizeToFit()
        optionsView.addSubview(distanceAnnounceSliderLabel)
        
        distanceAnnounceSliderLabel.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(optionsView)
            make.top.equalTo(distanceAnnounceSlider.snp_bottom).offset(10)
        }
        
        halfwayAnnounceLabel.text = "Use halfway alerts"
        halfwayAnnounceLabel.textAlignment = .Left
        optionsView.addSubview(halfwayAnnounceLabel)
        
        halfwayAnnounceLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.width.equalTo(150)
            make.left.equalTo(optionsView).offset(35)
            make.top.equalTo(distanceAnnounceSliderLabel.snp_bottom).offset(50)
        }
        
        halfwayAnnounceSwitch.on = prefs.boolForKey("UseHalfwayAlerts")
        halfwayAnnounceSwitch.addTarget(self, action: #selector(self.halfwayAnnounceSwitchAction(_:)), forControlEvents: .TouchUpInside)
        optionsView.addSubview(halfwayAnnounceSwitch)
        
        halfwayAnnounceSwitch.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(halfwayAnnounceLabel.snp_right).offset(5)
            make.centerY.equalTo(halfwayAnnounceLabel)
        }
        
        halfwayAnnounceSlider.minimumValue = 500
        halfwayAnnounceSlider.maximumValue = 5000
        halfwayAnnounceSlider.value = prefs.floatForKey("HalfwayAlert")
        halfwayAnnounceSlider.addTarget(self, action: #selector(self.halfwayAnnounceSliderAction(_:)), forControlEvents: .TouchDragInside)
        halfwayAnnounceSlider.hidden = true
        optionsView.addSubview(halfwayAnnounceSlider)
        
        halfwayAnnounceSlider.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(optionsView).offset(-120)
            make.centerX.equalTo(optionsView).offset(-12.5)
            make.top.equalTo(halfwayAnnounceLabel.snp_bottom).offset(15)
        }
        
        halfwayMinLabel.text = "500"
        halfwayMinLabel.textAlignment = .Center
        halfwayMinLabel.hidden = true
        optionsView.addSubview(halfwayMinLabel)
        
        halfwayMinLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(40)
            make.right.equalTo(halfwayAnnounceSlider.snp_left).offset(-3)
            make.top.equalTo(halfwayAnnounceSlider)
        }
        
        halfwayMaxLabel.text = "5000"
        halfwayMaxLabel.textAlignment = .Center
        halfwayMaxLabel.hidden = true
        optionsView.addSubview(halfwayMaxLabel)
        
        halfwayMaxLabel.snp_makeConstraints { (make) -> Void in
            make.width.height.equalTo(50)
            make.left.equalTo(halfwayAnnounceSlider.snp_right).offset(3)
            make.top.equalTo(halfwayAnnounceSlider)
        }
        
        halfwayAnnounceSliderLabel.textAlignment = .Center
        halfwayAnnounceSliderLabel.text = "\(halfwayAnnounceSlider.value) meters."
        halfwayAnnounceSliderLabel.hidden = true
        halfwayAnnounceSliderLabel.sizeToFit()
        optionsView.addSubview(halfwayAnnounceSliderLabel)
        
        halfwayAnnounceSliderLabel.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(optionsView)
            make.top.equalTo(halfwayAnnounceSlider.snp_bottom).offset(10)
        }
        
        speechLabel.textAlignment = .Left
        speechLabel.text = "Use voice alerts"
        optionsView.addSubview(speechLabel)
        
        speechLabel.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.width.equalTo(150)
            make.left.equalTo(optionsView).offset(35)
            make.top.equalTo(halfwayAnnounceSliderLabel.snp_bottom).offset(50)
        }
        
        speechSwitch.on = prefs.boolForKey("UseVoiceAlerts")
        speechSwitch.addTarget(self, action: #selector(self.voiceSwitchAction(_:)), forControlEvents: .TouchUpInside)
        optionsView.addSubview(speechSwitch)
        
        speechSwitch.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(speechLabel.snp_right).offset(5)
            make.centerY.equalTo(speechLabel)
        }
        
        if prefs.boolForKey("UseDistanceAlerts") == true {
            setDistanceLabelsHidden(false)
        }
        if prefs.boolForKey("UseHalfwayAlerts") == true {
            setHalfwayLabelsHidden(false)
        }
    }
    
    func voiceSwitchAction(sender: UISwitch!) {
        
    }
    
    // TO DO USE USERDEFAULTS
    func distanceAnnounceSwitchAction(sender: UISwitch!) {
        if sender.on == true {
            setDistanceLabelsHidden(false)
        } else {
            setDistanceLabelsHidden(true)
        }
    }
    
    func halfwayAnnounceSwitchAction(sender: UISwitch!) {
        if sender.on == true {
            setHalfwayLabelsHidden(false)
        } else {
            setHalfwayLabelsHidden(true)
        }
    }
    
    func setDistanceLabelsHidden(bool: Bool) {
        distanceAnnounceSlider.hidden = bool
        distanceMaxLabel.hidden = bool
        distanceMinLabel.hidden = bool
        distanceAnnounceSliderLabel.hidden = bool
    }
    
    func setHalfwayLabelsHidden(bool: Bool) {
        halfwayAnnounceSlider.hidden = bool
        halfwayMaxLabel.hidden = bool
        halfwayMinLabel.hidden = bool
        halfwayAnnounceSliderLabel.hidden = bool
    }
    
    func distanceAnnounceSliderAction(sender: UISlider!) {
        sender.value = getRoundedValue(250, value: sender.value)
        distanceAnnounceSliderLabel.text = "\(sender.value) meters."
    }
    
    func halfwayAnnounceSliderAction(sender: UISlider!) {
        sender.value = getRoundedValue(500, value: sender.value)
        halfwayAnnounceSliderLabel.text = "\(sender.value) meters."
    }
    
    func getRoundedValue(steps: Float, value: Float) -> Float {
        return round(value / steps) * steps
    }
    
    func navBarTitleButtonAction(sender: UIButton!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
