//
//  ViewControllerExtension.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 20/10/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
