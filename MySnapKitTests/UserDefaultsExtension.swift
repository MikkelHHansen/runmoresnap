//
//  UserDefaultsExtension.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 16/11/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation

extension NSUserDefaults {
    // check for first launch - only true on first invocation after app install, false on all further invocations
    static func isFirstLaunch() -> Bool {
        let firstLaunchFlag = "FirstLaunchFlag"
        let isFirstLaunch = NSUserDefaults.standardUserDefaults().stringForKey(firstLaunchFlag) == nil
        if (isFirstLaunch) {
            NSUserDefaults.standardUserDefaults().setObject("false", forKey: firstLaunchFlag)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        return isFirstLaunch
    }
}
