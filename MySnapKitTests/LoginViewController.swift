//
//  LoginViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 22/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    private var superview = UIView()
    private let bgColor = createUIColorFromIntValues(21, GInt: 141, BInt: 246, AInt: 255)
    private var loginField = UITextField()
    private var passwordField = UITextField()
    private var runWithoutLoginButton = UIButton()
    private var rememberMeSwitch = UISwitch()
    private var rememberMeLabel = UILabel()
    private var loginButton = UIButton()
    private var forgotPWButton = UIButton()
    private var logoView = UIView()
    private var userDefaults = NSUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginField.delegate = self
        passwordField.delegate = self
        self.hideKeyboardWhenTappedAround()
        superview = self.view
        superview.backgroundColor = bgColor
        
        logoView.backgroundColor = UIColor.grayColor()
        superview.addSubview(logoView)
        
        logoView.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(superview.bounds.height / 3)
            make.width.equalTo(superview.bounds.width * 0.75)
            make.top.equalTo(superview).offset(50)
            make.centerX.equalTo(superview)
        }
        
        loginField.placeholder = "Username"
        loginField.layer.cornerRadius = 10
        loginField.backgroundColor = UIColor.whiteColor()
        loginField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        loginField.autocorrectionType = .No
        superview.addSubview(loginField)
        
        loginField.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width / 2)
            make.height.equalTo(50)
            make.centerX.equalTo(superview)
            make.top.equalTo(logoView.snp_bottom).offset(12.5)
        }
        
        passwordField.secureTextEntry = true
        passwordField.placeholder = "Password"
        passwordField.layer.cornerRadius = 10
        passwordField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        passwordField.backgroundColor = UIColor.whiteColor()
        superview.addSubview(passwordField)
        
        passwordField.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width / 2)
            make.height.equalTo(50)
            make.centerX.equalTo(superview)
            make.top.equalTo(loginField.snp_bottom).offset(12.5)
        }
        
        runWithoutLoginButton.setTitle("Continue without logging in?", forState: .Normal)
        runWithoutLoginButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        runWithoutLoginButton.titleLabel?.textAlignment = .Center
        
        runWithoutLoginButton.addTarget(self, action: #selector(self.runWithoutLoginButtonAction(_:)), forControlEvents: .TouchUpInside)
        superview.addSubview(runWithoutLoginButton)
        
        runWithoutLoginButton.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.width.equalTo(superview)
            make.bottom.equalTo(superview).offset(-20)
        }
        
        rememberMeSwitch.on = true
        rememberMeSwitch.transform = CGAffineTransformMakeScale(0.90, 0.90)
        superview.addSubview(rememberMeSwitch)
        
        rememberMeSwitch.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(passwordField.snp_bottom).offset(10)
            make.left.equalTo(passwordField.snp_left)
        }
        
        rememberMeLabel.textAlignment = .Left
        rememberMeLabel.text = "Remember me?"
        rememberMeLabel.textColor = UIColor.blackColor()
        rememberMeLabel.font = rememberMeLabel.font.fontWithSize(13)
        superview.addSubview(rememberMeLabel)
        
        rememberMeLabel.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(rememberMeSwitch.snp_right).offset(5)
            make.top.equalTo(passwordField.snp_bottom).offset(10)
            make.height.equalTo(rememberMeSwitch)
            make.width.equalTo(100)
        }
        
        loginButton.setTitle("Login", forState: .Normal)
        loginButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        loginButton.titleLabel?.textAlignment = .Center
        loginButton.layer.cornerRadius = 10
        loginButton.backgroundColor = UIColor.greenColor()
        loginButton.addTarget(self, action: #selector(self.loginButtonAction(_:)), forControlEvents: .TouchUpInside)
        superview.addSubview(loginButton)
        
        loginButton.snp_makeConstraints { (make) -> Void in
            make.width.height.equalTo(passwordField)
            make.bottom.equalTo(runWithoutLoginButton.snp_top)
            make.centerX.equalTo(superview)
        }
        
        forgotPWButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        forgotPWButton.setTitle("Forgot password?", forState: .Normal)
        forgotPWButton.titleLabel?.textAlignment = .Left
        forgotPWButton.titleLabel?.font = forgotPWButton.titleLabel?.font.fontWithSize(13)
        forgotPWButton.addTarget(self, action: #selector(self.forgotPWButtonAction(_:)), forControlEvents: .TouchUpInside)
        superview.addSubview(forgotPWButton)
        
        forgotPWButton.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(rememberMeLabel)
            make.top.equalTo(rememberMeSwitch.snp_bottom)
            make.height.equalTo(rememberMeSwitch)
        }
    }
    
    func forgotPWButtonAction(sender: UIButton!) {
        // TO DO forgot pw button actions
    }
    
    func loginButtonAction(sender: UIButton!) {
        // TO DO login button actions
    }
    
    func runWithoutLoginButtonAction(sender: UIButton!) {
        // TO DO go to runviewcontroller
    }
    
    func createUserButtonAction(sender: UIButton!) {
        // TO DO create user - insert into userdefaults to begin with..
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        superview.endEditing(true)
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func createNewUserGUI() {
        let createUserView = UIView()
        
        createUserView.backgroundColor = bgColor
        superview.addSubview(createUserView)
        
        createUserView.snp_makeConstraints { (make) -> Void in
            make.size.equalTo(superview)
            make.center.equalTo(superview)
        }
    }
}
