//
//  IntervalView.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 12/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class IntervalView: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private var superview : UIView?
    weak var super_class : NewIntervalViewController?
    
    var pickerViewDataSource = [Int]()
    var segmentedDataSource = [String]()
    var timePickerView = UIPickerView()
    var intervalView = UIView()
    var mainView = UIView()
    var saveButton = UIButton()
    var segmentedTypeControl = UISegmentedControl()
    var cancelButton = UIButton()
    
    var intervalLength = 0
    var intervalType = 0
    var title = ""
    var interval: Interval?
    var isEditingExisting = -1
    
    override init() {
        super.init()
        fillDataSource()
        fillTypeSource()
        timePickerView.tag = 1
        interval = nil
    }
    
    func createViewGUI() {
        
        mainView.backgroundColor = UIColor.whiteColor()
        superview!.addSubview(mainView)
        
        mainView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview!.bounds.width)
            make.height.equalTo(superview!.bounds.height)
            make.top.equalTo(superview!.snp_top)
        }
        
        mainView.addSubview(timePickerView)
        
        timePickerView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview!.bounds.width)
            make.height.equalTo(superview!.bounds.height / 4)
            make.top.equalTo(superview!.snp_top).offset(50)
        }
        
        segmentedTypeControl = UISegmentedControl(items: segmentedDataSource)
        segmentedTypeControl.selectedSegmentIndex = 0
        segmentedTypeControl.addTarget(self, action: #selector(self.segmentedControlAction(_:)), forControlEvents: UIControlEvents.ValueChanged)
        mainView.addSubview(segmentedTypeControl)
        
        segmentedTypeControl.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview!.bounds.width - 40)
            make.height.equalTo(superview!.bounds.height / 6)
            make.top.equalTo(timePickerView.snp_bottom).offset(20)
            make.left.equalTo(superview!.snp_left).offset(20)
            make.right.equalTo(superview!.snp_right).offset(-20)
        }
        
        cancelButton.backgroundColor = UIColor.redColor()
        cancelButton.layer.cornerRadius = 15
        cancelButton.setTitle("Cancel", forState: .Normal)
        cancelButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        cancelButton.addTarget(self, action: #selector(self.cancelButtonAction(_:)), forControlEvents: .TouchUpInside)
        mainView.addSubview(cancelButton)
        
        cancelButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(150)
            make.height.equalTo(50)
            make.bottom.equalTo(superview!.snp_bottom).offset(-20)
            make.right.equalTo(superview!.snp_right).offset(-superview!.bounds.width / 2 + 75)
        }
        
        saveButton.backgroundColor = UIColor.greenColor()
        saveButton.layer.cornerRadius = 15
        saveButton.setTitle("Save", forState: .Normal)
        saveButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        saveButton.addTarget(self, action: #selector(self.saveButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        mainView.addSubview(saveButton)
        
        saveButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(150)
            make.height.equalTo(50)
            make.bottom.equalTo(cancelButton.snp_top).offset(-20)
            make.right.equalTo(superview!.snp_right).offset(-superview!.bounds.width / 2 + 75)
        }
        
        if isEditingExisting != -1 {
            setPickerRowsAndType(intervalType, length: intervalLength)
        }
    }
    
    func cancelButtonAction(sender: UIButton!) {
        super_class = nil
        superview = nil
        mainView.removeFromSuperview()
    }
    
    func saveButtonAction(sender: UIButton!) {
        if isEditingExisting != -1 {
            super_class!.intervals[isEditingExisting].length = intervalLength
            super_class!.intervals[isEditingExisting].type = getIntervalTypeString(intervalType)
            super_class!.intervals[isEditingExisting].title = constructTitle()
            super_class!.intervalTableView.reloadData()
            super_class = nil
            superview = nil
            mainView.removeFromSuperview()
        } else {
            setInterval(getIntervalTypeString(intervalType), length: intervalLength, title: constructTitle())
            super_class!.intervals.append(interval!)
            super_class!.intervalTableView.reloadData()
            super_class = nil
            superview = nil
            mainView.removeFromSuperview()
        }
    }
    
    func segmentedControlAction(sender: UISegmentedControl!) {
        intervalType = sender.selectedSegmentIndex
    }
    
    func setSuperView(view: UIView, s_class: NewIntervalViewController) {
        superview = view
        super_class = s_class
    }
    
    func fillTypeSource() {
        segmentedDataSource.append("Run")
        segmentedDataSource.append("Walk")
        segmentedDataSource.append("Bike")
        segmentedDataSource.append("Sprint")
    }
    
    func fillDataSource() {
        for num in 0...59 {
            pickerViewDataSource.append(num)
        }
    }
    
    func getInterval() -> Interval {
        return interval!
    }
    
    func setInterval(type: String, length: Int, title: String) {
        interval = Interval(type: type, length: length, title: title)
    }
    
    private func constructTitle() -> String {
        let minutes = String(format: "%02d", intervalLength / 60)
        let seconds = String(format: "%02d", intervalLength % 60)
        title = "\(getIntervalTypeString(intervalType)) \(minutes):\(seconds)"
        return title
    }
    
    func setIntervalData(type: String, length: Int, intervalIndex: Int) {
        intervalType = getIntervalTypeInt(type)
        intervalLength = length
        isEditingExisting = intervalIndex
    }
    
    func setPickerRowsAndType(type: Int, length: Int) {
        timePickerView.selectRow((length / 60), inComponent: 0, animated: true)
        timePickerView.selectRow((length % 60), inComponent: 1, animated: true)
        segmentedTypeControl.selectedSegmentIndex = type
    }
    
    private func getIntervalTypeInt(typeString: String) -> Int {
        switch typeString {
        case "Run":
            return 0
        case "Walk":
            return 1
        case "Bike":
            return 2
        case "Sprint":
            return 3
        default:
            return -1
        }
    }
    
    private func getIntervalTypeString(typeInt: Int) -> String {
        switch typeInt {
        case 0:
            return "Run"
        case 1:
            return "Walk"
        case 2:
            return "Bike"
        case 3:
            return "Sprint"
        default:
            return "Unknown Type"
        }
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewDataSource.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        intervalLength = pickerView.selectedRowInComponent(0) * 60 + pickerView.selectedRowInComponent(1)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        
        if pickerView.tag == 1 && component == 0 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataSource[row]) min"
            } else {
                pickerLabel.text = "\(pickerViewDataSource[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 22)
        } else if pickerView.tag == 1 && component == 1 {
            if row == 0 {
                pickerLabel.text = "\(pickerViewDataSource[row]) sec"
            } else {
                pickerLabel.text = "\(pickerViewDataSource[row])"
            }
            pickerLabel.textAlignment = NSTextAlignment.Center
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 22)
        }
        return pickerLabel
    }
    
    
}