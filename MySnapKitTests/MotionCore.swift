//
//  MotionCore.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 29/08/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import CoreMotion

class MotionCore : CMMotionManager {
    
    var motionManager = CMMotionManager()
    var timer = NSTimer()
    var x = 0.0, y = 0.0, z = 0.0
    
    func startAccelUpdates() {
        motionManager.accelerometerUpdateInterval = 0.01
        motionManager.startAccelerometerUpdates()
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(MotionCore.timertask(_:)), userInfo: nil, repeats: true)
    }
    
    func stopAccelUpdates() {
        motionManager.stopAccelerometerUpdates()
        timer.invalidate()
    }
    
    func timertask(timer:NSTimer){
        if let accelerometerData = motionManager.accelerometerData {
            x = accelerometerData.acceleration.x
            y = accelerometerData.acceleration.y
            z = accelerometerData.acceleration.z
            
            //print("x: \(x) y:\(y) z:\(z)")
            
        } else {
            print("No acc data")
        }
    }
}
