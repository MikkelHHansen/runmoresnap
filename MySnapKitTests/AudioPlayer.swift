//
//  AudioPlayer.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 17/11/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import AVFoundation

class AudioPlayer: NSObject, AVAudioPlayerDelegate {
    
    var audioPlayer: AVAudioPlayer?
    
    override init() {
        super.init()
    }
    
    func playDistanceAlertSound() {
        let url = NSBundle.mainBundle().URLForResource("distalert", withExtension: "mp3")!
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: url)
            guard let player = audioPlayer else { return }
            player.delegate = self
            player.prepareToPlay()
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    func playHalfwayAlertSound() {
        let url = NSBundle.mainBundle().URLForResource("halfalert", withExtension: "mp3")!
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: url)
            guard let player = audioPlayer else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    func playNextIntervalSound() {
        let url = NSBundle.mainBundle().URLForResource("DingSound", withExtension: "wav")!
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: url)
            guard let player = audioPlayer else { return }
            
            player.prepareToPlay()
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    func playSoundFile(name: String, ext: String) {
        let url = NSBundle.mainBundle().URLForResource(name, withExtension: ext)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: url!)
            guard let player = audioPlayer else { return }
            
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, withOptions: .DuckOthers)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player.delegate = self
            player.prepareToPlay()
            player.play()
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch let error as NSError {
            print(error)
        }
    }
}
