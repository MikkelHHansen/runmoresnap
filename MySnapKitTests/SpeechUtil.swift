//
//  SpeechUtil.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 22/11/2016.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import AVFoundation

class SpeechUtil: NSObject, AVSpeechSynthesizerDelegate {
    
    let synth = AVSpeechSynthesizer()
    
    override init() {
        super.init()
        synth.delegate = self
    }
    
    var utter = AVSpeechUtterance(string: "Polly, want a cracker?")
    // print(AVSpeechSynthesisVoice.speechVoices())
    let voice = AVSpeechSynthesisVoice(language: "en-US")
    
    func speak(utter: AVSpeechUtterance) {
        utter.voice = voice
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, withOptions: .DuckOthers)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
        synth.speakUtterance(utter)
    }
    
    func setUtterance(string: String) {
        utter = AVSpeechUtterance(string: string)
        utter.rate = 0.39
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, withOptions: .DuckOthers)
            try AVAudioSession.sharedInstance().setActive(true)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        } catch let error as NSError {
            print(error)
        }
    }
}
