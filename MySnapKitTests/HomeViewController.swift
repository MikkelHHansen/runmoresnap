//
//  HomeViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 22/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
