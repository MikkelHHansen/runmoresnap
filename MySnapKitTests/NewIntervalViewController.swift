//
//  NewIntervalViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 15/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class NewIntervalViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var intervals = [Interval]()
    var intervalRuns = [Run]()
    var timer = NSTimer()
    var gps = GPSCore()
    var menu = MainMenuView()
    
    var cellButtonHashValuesDictionary = [Int: Int]()
    var superview = UIView()
    var navBar = UIView()
    var navBarTitleButton = UIButton()
    var addIntervalButton = UIButton()
    var optionsButton = UIButton()
    var startButton = UIButton()
    var resetButton = UIButton()
    var intervalTableView = UITableView()
    var countdownLabel = UILabel()
    var intervalView: IntervalView?
    var currentInterval = 0
    var intervalLengthPlaceholder = [Int]()
    var isRunning: Bool?
    var isNewRun: Bool = true
    let prefs = NSUserDefaults.standardUserDefaults()
    var isOptionsGUIActive = false
    let optionsView = UIView()
//    var detailTopView = UIView()
    var detailView = UIView()
    let closeDetailButton = UIButton()
    let intervalTypeLabel = UILabel()
    let intervalDurationLabel = UILabel()
    let intervalDistanceLabel = UILabel()
    let intervalSpeedLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gps.calibrateGPS()
        
        // DEBUG VOICE ALERT
        prefs.setBool(true, forKey: "UseVoiceAlertsInterval")
        
        superview = self.view
        intervalTableView.delegate = self
        intervalTableView.dataSource = self
        intervalTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        superview.backgroundColor = UIColor.whiteColor()
        
        navBar.backgroundColor = UIColor.grayColor()
        superview.addSubview(navBar)
        
        navBar.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(60)
            make.top.equalTo(superview.snp_top)
        }
        
        navBarTitleButton.setTitle("RunMore", forState: .Normal)
        navBarTitleButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        navBarTitleButton.addTarget(self, action: #selector(self.navBarTitleButtonAction(_:)), forControlEvents: .TouchUpInside)
        navBarTitleButton.sizeToFit()
        navBar.addSubview(navBarTitleButton)
        
        navBarTitleButton.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(navBar)
        }
        
        addIntervalButton.setTitle("Add", forState: .Normal)
        addIntervalButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
        addIntervalButton.addTarget(self, action: #selector(NewIntervalViewController.addIntervalButtonAction(_:)), forControlEvents: .TouchUpInside)
        navBar.addSubview(addIntervalButton)
        
        addIntervalButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(50)
            make.height.equalTo(25)
            make.right.equalTo(navBar).offset(-5)
            make.bottom.equalTo(navBar).offset(-5)
        }
        
        /*
        //menuButton.setImage(UIImage(named: "menu_icon.png"), forState: .Normal)
        optionsButton.setTitle("Options", forState: .Normal)
        optionsButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
        optionsButton.sizeToFit()
        optionsButton.addTarget(self, action: #selector(NewIntervalViewController.optionsButtonAction(_:)), forControlEvents: .TouchUpInside)
        navBar.addSubview(optionsButton)
        
        optionsButton.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.left.equalTo(navBar).offset(5)
            make.bottom.equalTo(navBar).offset(-5)
        }
        */
        
        superview.addSubview(intervalTableView)
        
        intervalTableView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width - 40)
            make.height.equalTo(superview.bounds.height / 2)
            make.left.equalTo(0).offset(20)
            make.right.equalTo(0).offset(-20)
            make.top.equalTo(navBar.snp_bottom).offset(20)
        }
        
        startButton.layer.cornerRadius = 44
        startButton.backgroundColor = UIColor.greenColor()
        startButton.setTitle("Start", forState: .Normal)
        startButton.addTarget(self, action: #selector(NewIntervalViewController.startButtonAction(_:)), forControlEvents: .TouchUpInside)
        startButton.userInteractionEnabled = false
        superview.addSubview(startButton)
        
        startButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(startButton.layer.cornerRadius * 2)
            make.height.equalTo(startButton.layer.cornerRadius * 2)
            make.bottom.right.equalTo(0).offset(-20)
        }
        
        resetButton.layer.cornerRadius = 44
        resetButton.backgroundColor = UIColor.redColor()
        resetButton.setTitle("Reset", forState: .Normal)
        resetButton.addTarget(self, action: #selector(NewIntervalViewController.resetButtonAction(_:)), forControlEvents: .TouchUpInside)
        resetButton.userInteractionEnabled = false
        superview.addSubview(resetButton)
        
        resetButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(resetButton.layer.cornerRadius * 2)
            make.height.equalTo(resetButton.layer.cornerRadius * 2)
            make.left.equalTo(0).offset(20)
            make.bottom.equalTo(0).offset(-20)
        }
        
        countdownLabel.textAlignment = .Center
        countdownLabel.text = "00:00"
        countdownLabel.font = countdownLabel.font.fontWithSize(50)
        countdownLabel.font = countdownLabel.font?.monospacedDigitFont
        superview.addSubview(countdownLabel)
        
        countdownLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview.bounds.width - 40)
            make.height.equalTo(75)
            make.left.equalTo(0).offset(20)
            make.right.equalTo(0).offset(-20)
            make.top.equalTo(intervalTableView.snp_bottom).offset(10)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return intervals.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //Selected row
        if intervalRuns.count > 0 {
            constructDetailsGUI(intervalRuns[indexPath.row].distance, intervalType: intervals[indexPath.row].type, intervalDuration: intervals[indexPath.row].length)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        createCellButton(tableView, indexPath: indexPath, cell: cell)
        
        if indexPath.row == 0 {
            setCountdownLabelDefault()
        }
        startButton.userInteractionEnabled = true
        resetButton.userInteractionEnabled = true
        cell.textLabel?.text = intervals[indexPath.row].title
        cell.selectionStyle = .Default
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            intervals.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            if indexPath.row == 0 && intervals.isEmpty {
                countdownLabel.text = "00:00"
                startButton.userInteractionEnabled = false
                resetButton.userInteractionEnabled = false
            }
            // Reload table data when a row is deleted to update dictionary containing buttons and their indexes
            tableView.reloadData()
            //tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: .Automatic)
        }
    }
    
    func startButtonAction(sender: UIButton!) {
        fillIntervalLengthDataArray(isRunning)
        if isNewRun {
            intervalRuns.removeAll()
            isNewRun = false
        }
        
        if !isRunning! {
            intervalTableView.userInteractionEnabled = false
            isRunning! = true
            gps.startRunning()
            startButton.setTitle("Pause", forState: .Normal)
            timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(NewIntervalViewController.updateTime), userInfo: nil, repeats: true)
        } else if isRunning! {
            isRunning! = false
            gps.stopRunning()
            startButton.setTitle("Resume", forState: .Normal)
            timer.invalidate()
        }
    }
    
    func resetButtonAction(sender: UIButton!) {
        let index = NSIndexPath(forItem: currentInterval, inSection: 0)
        intervalTableView.deselectRowAtIndexPath(index, animated: true)
        timer.invalidate()
        intervalTableView.userInteractionEnabled = true
        isRunning = nil
        isNewRun = true
        currentInterval = 0
        setCountdownLabelDefault()
        gps.stopRunning()
        gps.flushData()
        gps.run.flushRun()
        print("GPS Loc count: \(gps.locationsToSave.count)")
        startButton.setTitle("Start", forState: .Normal)
    }
    
    func updateTime() {
        let index = NSIndexPath(forItem: currentInterval, inSection: 0)
        intervalTableView.selectRowAtIndexPath(index, animated: true, scrollPosition: UITableViewScrollPosition.Middle)
        intervalLengthPlaceholder[currentInterval] -= 1
        let minutes = String(format: "%02d", intervalLengthPlaceholder[currentInterval] / 60)
        let seconds = String(format: "%02d", intervalLengthPlaceholder[currentInterval] % 60)
        countdownLabel.text = "\(minutes):\(seconds)"
        
        //TO DO Announce end of interval
        if prefs.boolForKey("UseVoiceAlertsInterval") {
            if currentInterval == intervalLengthPlaceholder.count - 1 && intervalLengthPlaceholder[currentInterval] == 7 {
                let speech = SpeechUtil()
                //speech.utter.rate = 0.05
                speech.setUtterance("Stop in")
                speech.speak(speech.utter)
            } else if intervalLengthPlaceholder[currentInterval] == 7 && currentInterval < intervalLengthPlaceholder.count - 1{
                let speech = SpeechUtil()
                //speech.utter.rate = 0.05
                speech.setUtterance("\(intervals[currentInterval + 1].type) in")
                speech.speak(speech.utter)
                print("\(intervals[currentInterval].type) in")
            }
            
            if intervalLengthPlaceholder[currentInterval] < 6 && intervalLengthPlaceholder[currentInterval] > 0 {
                print("cd : \(intervalLengthPlaceholder[currentInterval])")
                let speech = SpeechUtil()
                //speech.utter.rate = 0.01
                speech.setUtterance("\(intervalLengthPlaceholder[currentInterval])")
                speech.speak(speech.utter)
            }
        }
        
        // Go to next interval when time is up
        goToNextInterval()
        
        // If no more intervals to run
        stopWhenLastInterval()
    }
    
    func getIntervalType(index: Int) -> String {
        return ""
    }
    
    func stopWhenLastInterval() {
        if currentInterval == intervalLengthPlaceholder.count {
            timer.invalidate()
            let index = NSIndexPath(forItem: (currentInterval - 1), inSection: 0)
            intervalTableView.deselectRowAtIndexPath(index, animated: true)
            currentInterval = 0
            isRunning = nil
            intervalTableView.userInteractionEnabled = true
            setCountdownLabelDefault()
            startButton.setTitle("Start", forState: .Normal)
            gps.stopRunning()
            gps.flushData()
            gps.run.flushRun()
            isNewRun = true
            for run in intervalRuns {
                print("Run Distance: \(run.distance)")
            }
            print("Total Distance: \(gps.totalDistance)")
        }
    }
    
    func goToNextInterval() {
        if intervalLengthPlaceholder[currentInterval] == 0 {
            // Play ding when moving to next interval ??
            let audio = AudioPlayer()
            audio.playNextIntervalSound()
            
            currentInterval += 1
            let run = Run()
            let dataSet = gps.getRunData("")
            run.distance = dataSet.distance
            run.locations = dataSet.locations
            run.duration = ""
            run.timestamp = NSDate()
            
            intervalRuns.append(run)
            print("UPDATE TIME GPS Loc count: \(gps.locationsToSave.count)")
            print("UPDATE TIME GPS Distance: \(gps.distanceTraveled)")
            print("APPENDED DATA SET DISTANCE: \(intervalRuns[currentInterval - 1].distance)")
            gps.flushData()
            gps.run.flushRun()
            if currentInterval != intervalLengthPlaceholder.count {
                intervalLengthPlaceholder[currentInterval] += 1
                gps.distanceTraveled = 0
            }
        }
    }
    
    func cellEditButtonAction(sender: UIButton!) {
        for (hashK, index) in cellButtonHashValuesDictionary {
            if sender.hashValue == hashK {
                if isRunning == nil {
                    intervalView = IntervalView()
                    intervalView!.setSuperView(superview, s_class: self)
                    intervalView!.setIntervalData(intervals[index].type, length: intervals[index].length, intervalIndex: index)
                    intervalView!.timePickerView.dataSource = intervalView
                    intervalView!.timePickerView.delegate = intervalView
                    intervalView!.createViewGUI()
                }
            }
        }
    }
    
    func addIntervalButtonAction(sender: UIButton!) {
        if isRunning == nil {
            intervalView = IntervalView()
            intervalView!.setSuperView(superview, s_class: self)
            intervalView!.timePickerView.dataSource = intervalView
            intervalView!.timePickerView.delegate = intervalView
            intervalView!.createViewGUI()
        }
    }
    
    func optionsButtonAction(sender: UIButton!) {
        if !isOptionsGUIActive {
            constructMenuGUI()
            isOptionsGUIActive = true
        } else {
            // set userdefaults
            
            
            optionsView.removeFromSuperview()
            isOptionsGUIActive = false
        }
        
    }
    
    func setCountdownLabelDefault() {
        let minutes = String(format: "%02d", intervals[0].length / 60)
        let seconds = String(format: "%02d", intervals[0].length % 60)
        countdownLabel.text = "\(minutes):\(seconds)"
    }
    
    func fillIntervalLengthDataArray(isRunning: Bool?) {
        if isRunning == nil {
            if !intervalLengthPlaceholder.isEmpty {
                intervalLengthPlaceholder.removeAll()
                for interval in intervals {
                    intervalLengthPlaceholder.append(interval.length)
                }
            } else {
                for interval in intervals {
                    intervalLengthPlaceholder.append(interval.length)
                }
            }
            self.isRunning = false
        }
    }
    
    func createCellButton(tableView: UITableView, indexPath: NSIndexPath, cell: UITableViewCell) {
        let cellEditButton = UIButton()
        cellButtonHashValuesDictionary[cellEditButton.hashValue] = indexPath.row
        cellEditButton.setTitle("Edit", forState: .Normal)
        cellEditButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        cellEditButton.addTarget(self, action: #selector(NewIntervalViewController.cellEditButtonAction(_:)), forControlEvents: .TouchUpInside)
        cell.addSubview(cellEditButton)
        
        cellEditButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(cell)
            make.right.equalTo(cell.snp_right).offset(-10)
        }
    }

    private func constructDetailsGUI(intervalLength: Double, intervalType: String, intervalDuration: Int) {
        
//        detailTopView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
//        superview.addSubview(detailTopView)
//        
//        detailTopView.snp_makeConstraints { (make) -> Void in
//            make.size.equalTo(superview)
//            make.center.equalTo(superview)
//        }
        
        detailView.backgroundColor = UIColor.grayColor()
        detailView.layer.cornerRadius = 11
        superview.addSubview(detailView)
        
        detailView.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(superview.bounds.height / 2.5)
            make.width.equalTo(superview.bounds.width / 1.5)
            make.center.equalTo(superview)
        }
        
        intervalTypeLabel.textAlignment = .Center
        intervalTypeLabel.text = "Type: \(intervalType)"
        detailView.addSubview(intervalTypeLabel)
        
        intervalTypeLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(detailView.snp_top).offset(10)
        }
        
        intervalDurationLabel.textAlignment = .Center
        intervalDurationLabel.text = "Duration: \(intervalDuration) sec."
        detailView.addSubview(intervalDurationLabel)
        
        intervalDurationLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(intervalTypeLabel.snp_bottom).offset(10)
        }
        
        intervalDistanceLabel.textAlignment = .Center
        intervalDistanceLabel.text = "Distance: \(intervalLength.roundToPlaces(2)) m."
        detailView.addSubview(intervalDistanceLabel)
        
        intervalDistanceLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(intervalDurationLabel.snp_bottom).offset(10)
        }
        
        let avgSpeed = intervalLength / Double(intervalDuration)
        intervalSpeedLabel.textAlignment = .Center
        intervalSpeedLabel.text = "Average speed: \(avgSpeed.roundToPlaces(1)) m/s."
        detailView.addSubview(intervalSpeedLabel)
        
        intervalSpeedLabel.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(detailView)
            make.height.equalTo(50)
            make.left.equalTo(detailView).offset(5)
            make.top.equalTo(intervalDistanceLabel.snp_bottom).offset(10)
        }
        
        closeDetailButton.setTitle("X", forState: .Normal)
        closeDetailButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        closeDetailButton.addTarget(self, action: #selector(NewIntervalViewController.closeDetailButtonAction(_:)), forControlEvents: .TouchUpInside)
        detailView.addSubview(closeDetailButton)
        
        closeDetailButton.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(detailView).offset(5)
            make.right.equalTo(detailView).offset(-5)
            make.width.height.equalTo(20)
        }
    }
    
    func closeDetailButtonAction(sender: UIButton!) {
        detailView.removeFromSuperview()
    }
    
    func navBarTitleButtonAction(sender: UIButton!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func constructMenuGUI() {
        optionsView.backgroundColor = UIColor.whiteColor()
        superview.addSubview(optionsView)
        
        optionsView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview)
            make.height.equalTo(superview).offset(-60)
            make.bottom.equalTo(superview)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
