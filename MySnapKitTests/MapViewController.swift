//
//  MapViewController.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 05/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    var superview = UIView()
    var mapView = MKMapView()
    let exitButton = UIButton()
    
    var intervals = [Interval]()
    var intervalRuns = [Run]()
    var run = Run()
    var location : Location!
    var myPolyLine = MKPolyline()
    var mapViewRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var useIntervals = false
    var currentColor = UIColor.whiteColor()
    
    
    func setRunData(run: Run) {
        self.run = run
    }
    
    func setCurrentIntervalColor(intervalType: String) {
        //let intervalType = intervals[index].type
        
        if intervalType == "Walk" {
            currentColor = UIColor.blackColor()
        } else if intervalType == "Run" {
            currentColor = UIColor.blueColor()
        } else if intervalType == "Bike" {
            currentColor = UIColor.greenColor()
        } else if intervalType == "Sprint" {
            currentColor = UIColor.redColor()
        } else {
            print("Something wrong with color shizzle")
        }
    }
    
    func setIntervalsAndRuns(intervals: [Interval], runs: [Run]) {
        self.intervals = intervals
        intervalRuns = runs
        useIntervals = true
    }
    
    func exitButtonAction(sender: UIButton!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.mapType = MKMapType.Hybrid
        superview = self.view
        
        
        mapView.backgroundColor = UIColor.greenColor()
        mapViewRect = CGRect(x: 0, y: 0, width: superview.bounds.width, height: superview.bounds.height)
        mapView.frame = mapViewRect
        superview.addSubview(mapView)
        configureView()
        
        exitButton.layer.cornerRadius = 33
        exitButton.setTitle("Exit", forState: .Normal)
        exitButton.backgroundColor = UIColor.greenColor()
        exitButton.addTarget(self, action: #selector(MapViewController.exitButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        mapView.addSubview(exitButton)
    
        exitButton.snp_makeConstraints { (make) -> Void in
            make.height.equalTo(66)
            make.width.equalTo(66)
            make.right.equalTo(mapView.snp_right).offset(-20)
            make.bottom.equalTo(mapView.snp_bottom).offset(-20)
            
        }
    }
    
    func numOfLinesToSkip(numOfLocations: Int) -> Int {
        return 0
    }
    
    func configureView() {
        if useIntervals {
            loadIntervalMap()
        } else {
            loadRunMap()
        }
    }
    
    func mapRegion() -> MKCoordinateRegion {
        let initialLoc = run.locations.first
        
        var minLat = initialLoc!.latitude
        var minLong = initialLoc!.longitude
        var maxLat = minLat
        var maxLong = minLong
        
        let locations = run.locations
        
        for location in locations {
            minLat = min(minLat, location.latitude)
            minLong = min(minLong, location.longitude)
            maxLat = max(maxLat, location.latitude)
            maxLong = max(maxLong, location.longitude)
        }
        
        return MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: (minLat + maxLat)/2,
                longitude: (minLong + maxLong)/2),
            span: MKCoordinateSpan(latitudeDelta: (maxLat - minLat)*1.1,
                longitudeDelta: (maxLong - minLong)*1.1))
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
//        if !overlay.isKindOfClass(MKPolyline) {
//            return nil
//        }
        
        myPolyLine = overlay as! MKPolyline
        let renderer = MKPolylineRenderer(polyline: myPolyLine)
        if useIntervals {
            renderer.strokeColor = currentColor
        } else {
            renderer.strokeColor = UIColor.blackColor()
        }
        renderer.lineWidth = 8
        return renderer
    }
    
    func polyline() -> MKPolyline {
        var coords = [CLLocationCoordinate2D]()
        
        let locations = run.locations
        for location in locations {
            coords.append(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        
        return MKPolyline(coordinates: coords, count: run.locations.count)
    }
    
    func loadRunMap() {
        if run.locations.count > 0 {
            mapView.hidden = false
            mapView.region = mapRegion()
            mapView.addOverlay(polyline())
        } else {
            print("No locations to render")
        }
    }
    
    func loadIntervalMap() {
        var currentRun = 0
        // TO DO - Traverse all runs / intervals - change line color depending on interval type
        for interval in intervals {
            setCurrentIntervalColor(interval.type)
            run = intervalRuns[currentRun]
            mapView.hidden = false
            if currentRun == 0 {
                mapView.region = mapRegion()
            }
            mapView.addOverlay(polyline())
            currentRun += 1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
