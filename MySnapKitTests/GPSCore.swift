//
//  GPSCore.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 29/08/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation
import CoreLocation

class GPSCore : NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var locationsToSave = [CLLocation]()
    var startLocation = CLLocation()
    var lastLocation = CLLocation()
    var distanceTraveled: Double = 0
    var totalDistance: Double = 0
    var run = Run()
    var timer = NSTimer()
    var secCounter = 0

    override init() {
        super.init()
        // locManager delegation and desired gps accuracy
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = CLActivityType.Fitness
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.allowsBackgroundLocationUpdates = true
    }
    
    /*
    Returns a Run object with all saved locations,
    and distance traveled during gps logging.
    */
    func getRunData(duration: String) -> Run {
        var savedLocations = [Location]()
        run.distance = distanceTraveled
        run.duration = duration
        run.timestamp = NSDate()
        
        for location in locationsToSave {
            let newLocation = Location()
            newLocation.timestamp = location.timestamp
            newLocation.latitude = location.coordinate.latitude
            newLocation.longitude = location.coordinate.longitude
            newLocation.speed = location.speed
            savedLocations.append(newLocation)
        }
        
        run.locations = savedLocations
        return run
    }
    
    // Used to start gps logging as well as device motion logging
    func startRunning() {
        locationManager.startUpdatingLocation()
    }
    
    // Used to stop gps logging as well as device motion logging
    func stopRunning() {
        locationManager.stopUpdatingLocation()
    }
    
    // Used to get an initial fix on the device location before starting gps logging
    func calibrateGPS() {
        timer  = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(GPSCore.updateTimer), userInfo: nil, repeats: true)
        startRunning()
    }
    
    // function fired every time a timer is updating and this is the selector
    func updateTimer() {
        secCounter += 1
        print("Calibrating gps.")
        if secCounter == 5 {
            timer.invalidate()
            stopRunning()
            run.flushRun()
        }
    }
    
    // used to remove all saved locations
    func flushLocations() {
        self.locationsToSave.removeAll()
    }
    
    // Removes all locations and resets distance
    func flushData() {
        self.locationsToSave.removeAll()
        distanceTraveled = 0
    }
    
    // function automatically called every time the gps gets a new location
    // Used to check locations for accuracy before being added to saved locations
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        startLocation = locations.last!
//        print("hAcc: \(locations.last?.horizontalAccuracy)")
//        print("Speed: \(locations.last?.speed)")
//        print("Timestamp: \(locations.last?.timestamp)")
        for location in locations {
            if location.horizontalAccuracy < 20 {
                lastLocation = location
                if self.locationsToSave.count > 0 {
                    distanceTraveled += location.distanceFromLocation(self.locationsToSave.last!)
                    totalDistance += location.distanceFromLocation(self.locationsToSave.last!)
                }
                
                self.locationsToSave.append(location)
            }
        }
    }
    
    // Called every time gps fails to obtain locations
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Error fetching GPS")
    }
    
    
}
