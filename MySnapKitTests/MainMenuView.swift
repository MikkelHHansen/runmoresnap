//
//  MainMenuView.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 15/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import UIKit
import SnapKit

class MainMenuView: NSObject {
    
    private var superview : UIView?
    private var buttonHeight : CGFloat?
    
    var runButton = UIButton()
    var intervalButton = UIButton()
    var menuView = UIView()
    
    override init() {
        super.init()
    }
    
    func setSuperView(view: UIView) {
        superview = view
    }
    
    func constructGUI() {
        
        menuView.backgroundColor = UIColor.whiteColor()
        superview!.addSubview(menuView)
        
        menuView.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(superview!.bounds.width / 2)
            make.height.equalTo(superview!)
            make.left.top.equalTo(0)
        }
        
        constructMenuItems()
    }
    
    func constructMenuItems() {
        buttonHeight = 50
        
        runButton.setTitle("Run", forState: .Normal)
        runButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        runButton.backgroundColor = UIColor.whiteColor()
        menuView.addSubview(runButton)
        
        runButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(menuView)
            make.height.equalTo(buttonHeight!)
            make.top.equalTo(menuView.snp_top)
        }
        
        intervalButton.setTitle("Interval", forState: .Normal)
        intervalButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        intervalButton.backgroundColor = UIColor.whiteColor()
        menuView.addSubview(intervalButton)
        
        intervalButton.snp_makeConstraints { (make) -> Void in
            make.width.equalTo(menuView)
            make.height.equalTo(buttonHeight!)
            make.top.equalTo(runButton.snp_bottom)
        }
    }
}
