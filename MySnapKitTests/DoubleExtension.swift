//
//  DoubleExtension.swift
//  MySnapKitTests
//
//  Created by Mikkel Honoré Hansen on 05/09/16.
//  Copyright © 2016 MHH. All rights reserved.
//

import Foundation

extension Double {
    // Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}
